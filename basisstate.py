import numpy as np



class BasisState:
    def __init__(self, M, N, ns=2):
        self.state = np.zeros((M, N, ns), dtype=int)
        self.shape = (M, N, ns)

    def fromarray(arr):
        sh = arr.shape
        if len(sh) == 2:
            M, N = sh
            ns = 1
        elif len(sh) == 3:
            M, N, ns = sh
        else:
            raise ValueError("Bad shape: {}".format(sh))

        bstate = BasisState(M, N, ns)
        if ns == 1 and arr.ndim == 2:
            bstate.state = arr[:, :, np.newaxis]
        else:
            bstate.state = arr
        bstate.validate()
        return bstate

    def random(M, N, ns, K):
        array = np.zeros((M, N, ns), dtype=int)
        for _ in range(K):
            flatindex = np.random.randint(M * N * ns)
            index = np.unravel_index(flatindex, (M, N, ns))
            while array[index] == 1:
                flatindex = np.random.randint(M * N * ns)
                index = np.unravel_index(flatindex, (M, N, ns))
            array[index] = 1


        assert np.sum(array) == K
        res = BasisState.fromarray(array)
        res.validate()
        return res

    def onehop_p(self, other):
        from itertools import product, chain
        if not other.shape == self.shape:
            raise ValueError("Must have same shape")

        M, N, ns = self.shape
        looplist = product(range(M), range(N), range(ns))
        looplist = chain.from_iterable(self.withhop(i, j, spin) for i, j, spin in looplist)

        for possible in looplist:
            if np.allclose(possible, other.state):
                return True

        return False
                    
    def neighbors(self, i, j, s):
        M, N, _ = self.shape
        ns = [(i - 1, j, s), (i + 1, j, s), (i, j - 1, s), (i, j + 1, s)]
        ns = list(filter(lambda t: t[0] >= 0 and t[0] < M and t[1] >= 0 and t[1] < N, ns))
        return ns

    def withhop(self, i, j, s):
        if self.state[i, j, s] != 1:
            return
        for i1, j1, s1 in self.neighbors(i, j, s):
            if self.state[i1, j1, s1] == 1:
                continue
            possible = self.state.copy()
            possible[i, j, s] -= 1
            possible[i1, j1, s1] += 1
            yield possible

    def validate(self):
        assert np.logical_and(self.state <= 1, self.state >= 0).all(), self.state

    def onehops(self):
        from itertools import chain, product
        M, N, ns = self.shape
        coords = product(range(M), range(N), range(ns))
        for i, j, s in coords:
            for x in self.withhop(i, j, s):
                yield BasisState.fromarray(x)
                    

        
# A class describing basis states
# It might be useful to formalize
# the description of basis states
# this way?

# A state is a MxNx2 array where
# M is the number of lattice sites
# in 1 direction, N in the other
# and 2 layers for the spin.

if __name__ == "__main__":
    bstate = BasisState.random(10, 10, 1, 4)
    # print(list(bstate.state[:, :, 0].reshape(-1)))
    # print("##############")
    for neighbor in bstate.onehops():
        # print("-------------")
        # print(list(neighbor.state[:, :, 0].reshape(-1)))
        assert bstate.onehop_p(neighbor)
        assert not np.allclose(bstate.state, neighbor.state)

    exit()
    

    s1 = np.array([[0, 0, 0],
                   [0, 1, 0],
                   [0, 0, 0]])
    s2 = np.array([[0, 1, 0],
                   [0, 0, 0],
                   [0, 0, 0]])
    
    s3 = np.array([[0, 0, 0],
                   [1, 0, 0],
                   [0, 0, 0]])
    
    s4 = np.array([[1, 0, 0],
                   [0, 0, 0],
                   [0, 0, 0]])

    s5 = np.array([[1, 0, 0],
                   [0, 0, 0],
                   [0, 1, 0]])

    s6 = np.array([[0, 0, 0],
                   [0, 0, 0],
                   [0, 1, 0]])

    s7 = np.array([[0, 0, 0],
                   [0, 0, 1],
                   [0, 0, 0]])



    arrs = [s1, s2, s3, s4, s5, s6, s7]
    states = [BasisState.fromarray(arr) for arr in arrs]

    # for p in states[0].withhop(1, 1, 0):
    #     print(p[:, :, 0])
    #     print("")

    # for p in states[1].withhop(0, 1, 0):
    #     print(p[:, :, 0])
    #     print("")

    # exit()
    # print(states[0].state[:, :, 0])
    # for p in states[2].withhop(1, 0, 0):
    #     print(p[:, :, 0])
    #     print(np.allclose(p, states[0].state))
    #     print("")
        
    # for p in states[0].withhop(0, 0, 0):
    #     print(p)

    # exit()
                
        
    # try:
    #     states[0].withhop(0, 0, 0)
    #     raise ValueError
    # except:
    #     pass
    # exit()
    for i1, st1 in enumerate(states):
        onehop = states[0].onehop_p(st1)
        onehop2 = st1.onehop_p(states[0])
        assert onehop == onehop2, "{}: {}, {}".format(i1, onehop, onehop2)
        print("For i1: {}, found onehop = {}".format(i1, onehop))

