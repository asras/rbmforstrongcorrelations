import numpy as np
from hubbard import Hubbard
from quantumstate import QuantumState
from utils import *
from basisstate import BasisState
import matplotlib.pyplot as plt

M = 2
N = 2
ns = 2
nhidden = 20
K = 3

gstate = QuantumState(M, N, ns, nhidden, K)

t = 1
U = 1
H = Hubbard(t, U)


def generate_state(current):
    nparts = np.sum(current.state)
    M, N, ns = current.state.shape

    newstate = BasisState.random(M, N, ns, K)

    return newstate

def mccalc(ham, qstate, generator, niter=500, nburn=200):
    M, N, ns, _, K = qstate.params
    currstate = BasisState.random(M, N, ns, K)

    for j in range(nburn):
        candidate = generator(currstate)
        r = np.random.rand()
        p = qstate.acceptance(candidate, currstate)
        if r < p:
            currstate = candidate

    avgH = 0
    avgOa = 0
    avgOW = 0
    avgOb = 0
    avgOaH = 0
    avgOWH = 0
    avgObH = 0
    
    for j in range(niter):
        candidate = generator(currstate)
        r = np.random.rand()
        p = qstate.acceptance(candidate, currstate)
        if r < p:
            currstate = candidate

        val = qstate.apply(currstate)
        hpartsum = ham.element(currstate, currstate)
        for neigh in currstate.onehops():
            hpartsum += ham.element(currstate, neigh) * qstate.apply(neigh) / val
        avgH += hpartsum
        
        da, dW, db = qstate.Ow(currstate)

        avgOa += np.conj(da)
        avgOW += np.conj(dW)
        avgOb += np.conj(db)

        avgOaH += np.conj(da) * hpartsum
        avgOWH += np.conj(dW) * hpartsum
        avgObH += np.conj(db) * hpartsum

    avgH /= niter
    avgOa /= niter
    avgOW /= niter
    avgOb /= niter
    avgOaH /= niter
    avgOWH /= niter
    avgObH /= niter

    da = 2 * np.real(avgOaH - avgH * avgOa)
    dW = 2 * np.real(avgOWH - avgH * avgOW)
    db = 2 * np.real(avgObH - avgH * avgOb)
    

    return avgH, da, dW, db





nrounds = 200
learning_rate = 0.025
avgH = 0
energies = []
for i in range(nrounds):
    if i == nrounds - 1:
        print(f"Iteration {i}/{nrounds}. Energy: {avgH}")
    else:
        print(f"Iteration {i}/{nrounds}. Energy: {avgH}", end="\r")
    avgH, da, dW, db = mccalc(H, gstate, generate_state)

    energies.append(avgH)

    gstate.a_q -= learning_rate * da
    gstate.W_qk -= learning_rate * dW
    gstate.b_k -= learning_rate * db



plt.plot(energies)
plt.show()

    
    
