import numpy as np
from basisstate import BasisState

def list_all(nstates, nparts):
    if nparts > nstates:
        return [[]]
    if nparts == nstates:
        return [[1] * nstates]
    if nparts == 0:
        return [[0] * nstates]
    if nstates == 0:
        return [[]]
    states = []
    for i in range(2):
        ostates = list_all(nstates - 1, nparts - i)
        states.extend([[i] + ostate for ostate in ostates])

    return states
        


def list_all_states(M, N, ns, K):
    states = list_all(M * N * ns, K)
    arrays = [np.array(state).reshape(ns, N, M).T for state in states]
    bstates = [BasisState.fromarray(arr) for arr in arrays]
    return bstates



if __name__ == "__main__":
    for x in list_all_states(2, 2, 2, 2):
        print(x.state.reshape(4, -1))
        print("--------------")
