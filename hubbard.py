import numpy as np
from basisstate import BasisState



class Hubbard:
    def __init__(self, t, U):
        self.t = t
        self.U = U

    def element(self, state1, state2):
        assert type(state1) == BasisState
        assert type(state2) == BasisState

        if state1.onehop_p(state2):
            return -self.t
        elif np.allclose(state1.state, state2.state):
            thing = self.U * np.sum(np.product(state1.state, axis=-1))
            assert thing.ndim == 0
            return thing
        else:
            return 0.0

    


# A class describing the 2D Hubbard model Hamiltonian


if __name__ == "__main__":
    myarr = np.array([[[0, 0], [1, 0], [0, 0]],
                      [[0, 1], [0, 0], [0, 0]]])
    bstate = BasisState.fromarray(myarr)
    myarr2 = np.array([[[0, 0], [1, 1], [0, 0]],
                       [[0, 1], [0, 0], [0, 0]]])
    bstate2 = BasisState.fromarray(myarr2)
    myarr3 = np.array([[[1, 0], [0, 1], [0, 0]],
                       [[0, 1], [0, 0], [0, 0]]])
    bstate3 = BasisState.fromarray(myarr3)
    assert not bstate.onehop_p(bstate)
    assert not bstate2.onehop_p(bstate)
    assert bstate2.onehop_p(bstate3)
    H = Hubbard(1, 1)
    assert np.allclose(H.element(bstate, bstate), 0)
    assert np.allclose(H.element(bstate2, bstate2), 1)
    assert np.allclose(H.element(bstate2, bstate3), -1)
    assert np.allclose(H.element(bstate3, bstate3), 0)
