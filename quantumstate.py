import numpy as np
from basisstate import BasisState
from fermiontree import fermi_sea_expansions, detcoeff, positionstate_to_choices



class QuantumState:
    def __init__(self, M, N, ns, nhidden, K=0):
        Nstates = M * N * ns
        self.params = (M, N, ns, nhidden, K)
        self.Nstates = Nstates
        self.nhidden = nhidden
        self.shape = (M, N, ns, nhidden)
        self.W_qk = (np.random.rand(M, N, ns, nhidden).reshape(Nstates, nhidden) - 0.5) / np.sqrt(Nstates)**2 * 0.5
        self.b_k = (np.random.rand(nhidden) - 0.5) / np.sqrt(nhidden)**2 * 0.5
        self.a_q = (np.random.rand(Nstates) - 0.5) / np.sqrt(Nstates)**2 * 0.5
        self.K = K
        self.fermi_exps = fermi_sea_expansions(K, M, N, ns)
    
    def rbm(self, state):
        pseudo_q = self.to_pseudo_state(state)

        pre = np.prod(np.cosh(self.b_k + pseudo_q.dot(self.W_qk)))

        return pre * np.exp(self.a_q.dot(pseudo_q))

    def to_pseudo_state(self, state):
        assert type(state) == BasisState
        arr_ijs = state.state
        
        def to_pseudo_spin(x):
            return 2 * x - 1

        pseudo_ijs = to_pseudo_spin(arr_ijs)
        pseudo_q = pseudo_ijs.reshape(self.Nstates)

        return pseudo_q

    def rbm_derivatives(self, state):
        pseudo_q = self.to_pseudo_state(state)

        # Return drbm/da, drbm/dw, drbm/db
        da = self.rbm(state) * pseudo_q


        dw = np.zeros((self.Nstates, self.nhidden))
        for i in range(self.Nstates):
            for j in range(self.nhidden):
                val = 1
                for k in range(self.nhidden):
                    if k != j:
                        val *= np.cosh(self.b_k[k] + (pseudo_q.dot(self.W_qk))[k])
                    else:
                        val *= np.sinh(self.b_k[j] + (pseudo_q.dot(self.W_qk))[j])
                dw[i, j] = val * pseudo_q[i]

        dw *= np.exp(self.a_q.dot(pseudo_q))

        db = np.zeros(self.nhidden)
        for j in range(self.nhidden):
            val = 1
            for k in range(self.nhidden):
                if k != j:
                    val *= np.cosh(self.b_k[k] + (pseudo_q.dot(self.W_qk))[k])
                else:
                    val *= np.sinh(self.b_k[j] + (pseudo_q.dot(self.W_qk))[j])
            db[j] = val

        db *= np.exp(self.a_q.dot(pseudo_q))
        
        return da, dw, db

    def Ow(self, bstate):
        da, dw, db = self.rbm_derivatives(bstate)
        val = self.rbm(bstate)
        return da / val, dw / val, db / val


    def apply(self, bstate):
        choices = positionstate_to_choices(bstate)
        coeff = detcoeff(self.fermi_exps, choices)
        coeff = 1
        return coeff * self.rbm(bstate)

    def acceptance(self, n1, n2):
        val1 = self.apply(n1)
        val2 = self.apply(n2)

        if np.allclose(val2, 0):
            return 1

        return np.abs(val1 / val2)**2


    def copy(self):
        other = QuantumState(*self.params)
        other.W_qk = self.W_qk.copy()
        other.a_q = self.a_q.copy()
        other.b_k = self.b_k.copy()
        return other


class TIQuantumState:
    def __init__(self, M, N, ns, nhidden, K=0):
        Nstates = M * N * ns
        self.params = (M, N, ns, nhidden, K)
        self.ns = ns
        self.Nstates = Nstates
        self.nhidden = nhidden
        self.shape = (ns, nhidden)
        self.W_sk = (np.random.rand(ns, nhidden) - 0.5) / np.sqrt(nhidden)**2 * 0.5
        self.b_k = (np.random.rand(nhidden) - 0.5) / np.sqrt(nhidden)**2 * 0.5
        self.K = K
        self.fermi_exps = fermi_sea_expansions(K, M, N, ns)
    
    def rbm(self, state):
        pseudo_ijs = self.to_pseudo_state(state)

        pre = np.prod(np.cosh(self.b_k + np.sum(pseudo_ijs.dot(self.W_sk), axis=(0, 1))))

        return pre

    def to_pseudo_state(self, state):
        assert type(state) == BasisState
        arr_ijs = state.state
        
        def to_pseudo_spin(x):
            return 2 * x - 1

        pseudo_ijs = to_pseudo_spin(arr_ijs)

        return pseudo_ijs

    def rbm_derivatives(self, state):
        pseudo_ijs = self.to_pseudo_state(state)

        # Return drbm/dw, drbm/db


        dw = np.zeros((self.ns, self.nhidden))
        for i in range(self.ns):
            for j in range(self.nhidden):
                val = 1
                for k in range(self.nhidden):
                    if k != j:
                        val *= np.cosh(self.b_k[k] + np.sum(pseudo_ijs.dot(self.W_sk), axis=(0, 1))[k])
                    else:
                        val *= np.sinh(self.b_k[j] + np.sum(pseudo_ijs.dot(self.W_sk), axis=(0, 1))[j])
                dw[i, j] = val * np.sum(pseudo_ijs[:, :, i])


        db = np.zeros(self.nhidden)
        for j in range(self.nhidden):
            val = 1
            for k in range(self.nhidden):
                if k != j:
                    val *= np.cosh(self.b_k[k] + np.sum(pseudo_ijs.dot(self.W_sk), axis=(0, 1))[k])
                else:
                    val *= np.sinh(self.b_k[j] + np.sum(pseudo_ijs.dot(self.W_sk), axis=(0, 1))[j])
            db[j] = val
        
        return dw, db

    def Ow(self, bstate):
        dw, db = self.rbm_derivatives(bstate)
        val = self.rbm(bstate)
        return dw / val, db / val

    def apply(self, bstate):
        choices = positionstate_to_choices(bstate)
        choices = sorted(choices)
        coeff = detcoeff(self.fermi_exps, choices)
        # coeff = 1
        return coeff * self.rbm(bstate)

    def acceptance(self, n1, n2):
        val1 = self.apply(n1)
        val2 = self.apply(n2)

        if np.allclose(val2, 0):
            return 1

        return np.abs(val1 / val2)**2

    def copy(self):
        other = TIQuantumState(*self.params)
        other.W_sk = self.W_sk.copy()
        other.b_k = self.b_k.copy()
        return other
    

# QuantumState consists of two parts:
# 1. A reference state describing "base correlations".
## We need to be able to find <x|phi_ref> for some
## choice of basis states x
# 2. A RBM describing deviations from the reference state.





if __name__ == "__main__":
    M, N, ns, nhidden = 10, 10, 2, 20

    qstate = QuantumState(M, N, ns, nhidden)
    bstate = BasisState.random(M, N, ns, 5)
    print(qstate.rbm(bstate))

    def manual_dw(state):
        pseudo_q = qstate.to_pseudo_state(state)
        dw = np.zeros((qstate.Nstates, qstate.nhidden))

        for i in range(qstate.Nstates):
            for j in range(qstate.nhidden):
                val = 1
                for k in range(qstate.nhidden):
                    if k != j:
                        val *= np.cosh(qstate.b_k[k] + (pseudo_q.dot(qstate.W_qk))[k])
                    else:
                        val *= np.sinh(qstate.b_k[j] + (pseudo_q.dot(qstate.W_qk))[j])
                dw[i, j] = val * pseudo_q[i]

        dw *= np.exp(qstate.a_q.dot(pseudo_q))

        return dw

    da, dw, db = qstate.rbm_derivatives(bstate)
    assert np.allclose(manual_dw(bstate), dw), np.mean(np.abs(manual_dw(bstate) - dw))


    def random_index(shape):
        index = []
        for n in shape:
            index.append(np.random.randint(n))
        return tuple(index)
                        

    # FD test of derivatives
    for _ in range(10):
        qstate2 = qstate.copy()
        index = random_index(qstate2.W_qk.shape)
        dw = 0.0000001
        qstate2.W_qk[index] += dw
        
        val1 = qstate2.rbm(bstate)
        val0 = qstate.rbm(bstate)

        fd = (val1 - val0) / dw
        deriv = qstate.rbm_derivatives(bstate)[1]
        assert deriv.shape == qstate.W_qk.shape

        assert np.allclose(fd, deriv[index], atol=1e-3), fd / deriv[index]


        qstate2 = qstate.copy()
        index = random_index(qstate2.b_k.shape)
        db = 0.00000001
        qstate2.b_k[index] += db
        
        val1 = qstate2.rbm(bstate)
        val0 = qstate.rbm(bstate)

        fd = (val1 - val0) / db
        deriv = qstate.rbm_derivatives(bstate)[2]
        assert deriv.shape == qstate.b_k.shape

        assert np.allclose(fd, deriv[index], atol=1e-3), fd / deriv[index]


        qstate2 = qstate.copy()
        index = random_index(qstate2.a_q.shape)
        db = 0.00000001
        qstate2.a_q[index] += db
        
        val1 = qstate2.rbm(bstate)
        val0 = qstate.rbm(bstate)

        fd = (val1 - val0) / db
        deriv = qstate.rbm_derivatives(bstate)[0]
        assert deriv.shape == qstate.a_q.shape

        assert np.allclose(fd, deriv[index], atol=1e-3), fd / deriv[index]

    tistate = TIQuantumState(M, N, ns, nhidden)
    for _ in range(10):
        tistate2 = tistate.copy()
        index = random_index(tistate2.W_sk.shape)
        dw = 0.0000001
        tistate2.W_sk[index] += dw
        
        val1 = tistate2.rbm(bstate)
        val0 = tistate.rbm(bstate)

        fd = (val1 - val0) / dw
        deriv = tistate.rbm_derivatives(bstate)[0]
        assert deriv.shape == tistate.W_sk.shape

        assert np.allclose(fd, deriv[index], atol=1e-3, rtol=10), fd / deriv[index]


        tistate2 = tistate.copy()
        index = random_index(tistate2.b_k.shape)
        db = 0.00000001
        tistate2.b_k[index] += db
        
        val1 = tistate2.rbm(bstate)
        val0 = tistate.rbm(bstate)

        fd = (val1 - val0) / db
        deriv = tistate.rbm_derivatives(bstate)[1]
        assert deriv.shape == tistate.b_k.shape

        assert np.allclose(fd, deriv[index], atol=1e-3, rtol=10), fd / deriv[index]

    

    

