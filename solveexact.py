from utils import list_all_states
from hubbard import Hubbard
import numpy as np
import matplotlib.pyplot as plt
from basisstate import BasisState

M = 2
N = 2
ns = 2
K = 3

alls = list_all_states(M, N, ns, K)
nstates = len(alls)
print("nstates = ", nstates)
# for state in alls:
#     print(state.state.reshape(M * N, ns))
#     print("---")

sols = []
for U in [0, 1, 10000]:
    H = np.zeros((nstates, nstates))
    t = 1
    # U = 1
    hubham = Hubbard(t, U)

    for i1, state1 in enumerate(alls):
        for i2, state2 in enumerate(alls):
            # if np.sum(np.product(state2.state, axis=-1)) > 0 and i1 == i2:
            #     H[i1, i2] = 100000
            # else:
            H[i1, i2] = hubham.element(state1, state2)

    assert np.allclose(H, H.T.conj())

    vals, vecs = np.linalg.eigh(H)
    plt.plot(vals)
    plt.show()
    print("U = ", U)
    print("Lowest 5:", vals[:5])
    print("-------")
    twoelecsprob = np.zeros(M * N)
    density = np.zeros(M * N)
    for i, coeff in enumerate(vecs[:, 0]):
        twoelecsprob += coeff**2 * (alls[i].state[:, :, 0] * alls[i].state[:, :, 1]).reshape(-1)
        density += coeff**2 * (alls[i].state[:, :, 0] + alls[i].state[:, :, 1]).reshape(-1)
        
    sols.append(vals)

    # plt.subplot(311, label="blah")
    # plt.plot(vals, 'x', label="U = {}".format(U))
    # plt.subplot(312, label="blah")
    # plt.plot(twoelecsprob, label="U = {}".format(U))
    # plt.title("Two elecs prob")
    # # plt.plot((stateup - statedown) / (stateup + statedown), label="pol, U = {}".format(U))
    # # plt.plot((stateup2 - statedown2) / (stateup2 + statedown2), label="pol2, U = {}".format(U))
    # plt.subplot(313, label="blah")
    # plt.plot(density, label="U = {}".format(U))
    # plt.title("Electron density")
    # # plt.plot(statedown**2, label="down, U = {}".format(U))
    # # plt.plot(statedown2**2, label="down2, U = {}".format(U))

ac = True
for sol in sols[1:]:
    ac = ac and np.allclose(sols[0], sol)
print("Allclose?", ac)
# plt.legend()
# plt.show()
