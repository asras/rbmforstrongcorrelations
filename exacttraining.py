import numpy as np
from utils import list_all_states
from quantumstate import *
from hubbard import Hubbard
from fermiontree import detcoeff, positionstate_to_choices
# Perform exact training of QuantumState
# i.e. sum over all possible states to evaluate
# the average of H and the derivatives of this
# exactly. Training should then be almost noiseless
# and reach the ground state energy eventually.

M = 2
N = 2
ns = 2
nhidden = 20
K = 3

tistate = TIQuantumState(M, N, ns, nhidden, K)
gstate = QuantumState(M, N, ns, nhidden, K)

t = 1
U = 1
H = Hubbard(t, U)

def sumall_avgH(qstate, ham):
    M, N, ns, _, K = qstate.params
    avg = 0
    norm = 0
    alls = list_all_states(M, N, ns, K)
    nonzeros = []
    for i1, state1 in enumerate(alls):
        norm += np.abs(qstate.apply(state1))**2
        for i2, state2 in enumerate(alls):
            print("loop1: {}/{}, loop2: {}/{}".format(i1, len(alls), i2, len(alls)), end="\r")
            val1 = qstate.apply(state1)
            val2 = qstate.apply(state2)
            element = np.conj(val1) * ham.element(state1, state2) * val2
            if not np.allclose(ham.element(state1, state2), 0):
                nonzeros.append((state1, state2))
            avg += element

    return avg / norm, nonzeros

def sumrelevant_avgH(qstate, ham):
    M, N, ns, _, K = qstate.params
    avgH = 0
    norm = 0
    alls = list_all_states(M, N, ns, K)
    visited = []
    for i, state in enumerate(alls):
        norm += np.abs(qstate.apply(state))**2
        avgH += ham.element(state, state) * np.abs(qstate.apply(state))**2
        val1 = qstate.apply(state)
        if not np.allclose(ham.element(state, state), 0):
            visited.append((state, state))
        for neigh in state.onehops():
            visited.append((state, neigh))
            val2 = qstate.apply(neigh)
            avgH += np.conj(val1) * ham.element(state, neigh) * val2

    return avgH / norm, visited


def sumall_dH(qstate, ham):
    M, N, ns, _, K = qstate.params
    avgdW = 0
    avgdb = 0
    norm = 0
    alls = list_all_states(M, N, ns, K)
    avgH, _ = sumrelevant_avgH(qstate, ham)
    
    for i1, state1 in enumerate(alls):
        norm += np.abs(qstate.apply(state1))**2
        val1 = qstate.apply(state1)
        OW, Ob = qstate.Ow(state1)
        avgdW += - avgH * OW * np.abs(qstate.apply(state1))**2
        avgdb += - avgH * Ob * np.abs(qstate.apply(state1))**2
        for i2, state2 in enumerate(alls):
            val2 = qstate.apply(state2)
            Hel = ham.element(state1, state2)
            avgdW += OW * np.conj(val1) * Hel * val2
            avgdb += Ob * np.conj(val1) * Hel * val2

    return 2 * np.real(avgdW / norm), 2 * np.real(avgdb / norm)

def sumrelevant_dH(qstate, ham):
    M, N, ns, _, K = qstate.params
    avgdW = 0
    avgdb = 0
    norm = 0
    alls = list_all_states(M, N, ns, K)
    avgH, _ = sumrelevant_avgH(qstate, ham)
    # assert not np.allclose(avgH, 0)
    
    for i, state in enumerate(alls):
        norm += np.abs(qstate.apply(state))**2
        OW, Ob = qstate.Ow(state)
        OW = np.conj(OW)
        Ob = np.conj(Ob)
        # assert not np.allclose(OW, 0)
        # assert not np.allclose(Ob, 0)
        avgdW += - avgH * OW * np.abs(qstate.apply(state))**2
        avgdb += - avgH * Ob * np.abs(qstate.apply(state))**2
        Hel = ham.element(state, state)
        # assert np.allclose(Hel, 0), state.state
        avgdW += OW * Hel * np.abs(qstate.apply(state))**2
        avgdb += Ob * Hel * np.abs(qstate.apply(state))**2

        val = qstate.apply(state)
        # if not np.allclose(val, 0):
        #     print(np.sum(np.product(state.state, axis=-1)))
        #     choices = positionstate_to_choices(state)
        #     coeff = detcoeff(qstate.fermi_exps, choices)
        #     print(coeff)
        #     print("---------")

        for neigh in state.onehops():
            Hel = ham.element(state, neigh)
            assert not np.allclose(Hel, 0)            
            avgdW += OW * np.conj(val) * Hel * qstate.apply(neigh)
            avgdb += Ob * np.conj(val) * Hel * qstate.apply(neigh)

    # assert not np.allclose(avgdW, 0)
    # assert not np.allclose(avgdb, 0)
            
    return 2 * np.real(avgdW / norm), 2 * np.real(avgdb / norm), norm, avgH

def train(qstate, ham):
    niter = 100
    learning_rate = 0.1
    for it in range(niter):
        dW, db, norm, avgH = sumrelevant_dH(qstate, ham)
        assert dW.shape == qstate.W_sk.shape
        assert db.shape == qstate.b_k.shape
        # print(np.allclose(dW, 0))
        # print(np.allclose(db, 0))
        qstate.W_sk -= learning_rate * dW
        qstate.b_k -= learning_rate * db
        print("Iteration {}/{}, energy: {}, norm: {}".format(it, niter, avgH, norm), end="\r")


def gsumrelevant_dH(qstate, ham):
    M, N, ns, _, K = qstate.params
    avgdW = 0
    avgdb = 0
    avgda = 0
    norm = 0
    alls = list_all_states(M, N, ns, K)
    avgH, _ = sumrelevant_avgH(qstate, ham)
    # assert not np.allclose(avgH, 0)
    
    for i, state in enumerate(alls):
        norm += np.abs(qstate.apply(state))**2
        Oa, OW, Ob = qstate.Ow(state)
        Oa = np.conj(Oa)
        OW = np.conj(OW)
        Ob = np.conj(Ob)
        # assert not np.allclose(OW, 0)
        # assert not np.allclose(Ob, 0)
        avgda += - avgH * Oa * np.abs(qstate.apply(state))**2
        avgdW += - avgH * OW * np.abs(qstate.apply(state))**2
        avgdb += - avgH * Ob * np.abs(qstate.apply(state))**2
        Hel = ham.element(state, state)
        # assert np.allclose(Hel, 0), state.state
        avgda += Oa * Hel * np.abs(qstate.apply(state))**2
        avgdW += OW * Hel * np.abs(qstate.apply(state))**2
        avgdb += Ob * Hel * np.abs(qstate.apply(state))**2

        val = qstate.apply(state)
        # if not np.allclose(val, 0):
        #     print(np.sum(np.product(state.state, axis=-1)))
        #     choices = positionstate_to_choices(state)
        #     coeff = detcoeff(qstate.fermi_exps, choices)
        #     print(coeff)
        #     print("---------")

        for neigh in state.onehops():
            Hel = ham.element(state, neigh)
            assert not np.allclose(Hel, 0)
            avgda += Oa * np.conj(val) * Hel * qstate.apply(neigh)
            avgdW += OW * np.conj(val) * Hel * qstate.apply(neigh)
            avgdb += Ob * np.conj(val) * Hel * qstate.apply(neigh)

    # assert not np.allclose(avgdW, 0)
    # assert not np.allclose(avgdb, 0)
            
    return 2 * np.real(avgda / norm), 2 * np.real(avgdW / norm), 2 * np.real(avgdb / norm), norm, avgH


def gtrain(qstate, ham):
    niter = 500
    learning_rate = 0.05
    energies = []
    for it in range(niter):
        da, dW, db, norm, avgH = gsumrelevant_dH(qstate, ham)
        assert dW.shape == qstate.W_qk.shape
        assert db.shape == qstate.b_k.shape
        # print(np.allclose(dW, 0))
        # print(np.allclose(db, 0))
        qstate.a_q -= learning_rate * da
        qstate.W_qk -= learning_rate * dW
        qstate.b_k -= learning_rate * db
        print("Iteration {}/{}, energy: {}, norm: {}".format(it, niter, avgH, norm), end="\r")
        if it == niter - 1:
            print("Iteration {}/{}, energy: {}, norm: {}".format(it, niter, avgH, norm))
        energies.append(np.real(avgH))

    import matplotlib.pyplot as plt
    plt.plot(energies)
    plt.show()
        








if __name__ == "__main__":
    gtrain(gstate, H)
    exit()
    train(tistate, H)
    exit()
    
    dHdW, dHdb, _, _ = sumall_dH(tistate, H)
    dHdW2, dHdb2, _, _ = sumrelevant_dH(tistate, H)
    assert np.allclose(dHdW, dHdW2), "W ratio: {}, b ratio: {}".format(dHdW / dHdW2, dHdb, dHdb2)
    assert np.allclose(dHdb, dHdb2), "W ratio: {}, b ratio: {}".format(dHdW / dHdW2, dHdb, dHdb2)
    
    val1, nonzeros = sumall_avgH(tistate, H)
    val2, visited = sumrelevant_avgH(tistate, H)


    print("Length nonzeros: {}".format(len(nonzeros)))
    print("Length visited: {}".format(len(visited)))

    for i, (state1, state2) in enumerate(visited):
        print("Iteration: {}/{}".format(i, len(visited)), end="\r")
        found = False
        for n1, n2 in nonzeros:
            if np.allclose(state1.state, n1.state) and np.allclose(state2.state, n2.state):
                if found:
                    raise ValueError
                found = True

        if not found:
            print("Iteration: {}/{}".format(i, len(visited)))
            print("Did not find:\n{}\n{}\n".format(state1.state, state2.state))
            raise ValueError

    print("")
    assert np.allclose(val1, val2), "val1: {}, val2: {}".format(val1, val2)
