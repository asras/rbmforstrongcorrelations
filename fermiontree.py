# We have to be able to calculate <x|phi_fermisea>
# Where phi_fermisea is made from adding N particles
# to the vacuum with the N lowest magnitude momenta
# x is a state in the position basis |x> = \prod_q c_iqjq |0>,
# where c_ij creates a fermion at position i,j.
# We assume that the product of operators is always ordered
# lexicographically, such that the lowest operators in the
# ordering are closest to the vacuum state.

# In 1D the fermi sea is
# |FS> = cdagger_kN....cdagger_k2 cdagger_k1 cdagger_k0|0>
# This is a superposition of many different
# position eigenstates with different factors for each
# Suppose we want to find the coefficient for a
# given state |x>. 
# Naively we can represent the FS as a sort of tree
# The first branching corresponds to a choice for 
# cdagger_kN = \sum_i e^ikNxi cdagger_i,
# the second branching to a choice for cdagger_kN-1
# and so on. 
# When we want to calculate <x|FS> |x> will represent
# a set of branch choices. We need to apply the choices
# in all the different possible ways, remembering
# to keep track of the sign factors that can arise due
# to particle interchange antisymmetry.



# Quick test/improvisation
import numpy as np
xs = np.arange(4)
ck0 = np.exp(1j * 0 * xs)
ck1 = np.exp(1j * 1 * xs)
ck2 = np.exp(1j * 2 * xs)


# Fermi sea: ck2 ck1 ck0 |0>
# We want to find overlap with, say, |1,1,1,0>
# i.e. one particle at position 0, 1, and 2.

# So we need to choose
choices = [0, 1, 2]

def coeff(operator_expansions, choices):
    if len(operator_expansions) == 0 or len(choices) == 0:
        return int(len(operator_expansions) == 0 and len(choices) == 0)

    choices = sorted(choices)

    val = 0
    for i, choice in enumerate(choices):
        other_choices = choices.copy()
        other_choices.pop(i)

        # Account for fermion antisymmetry?
        if len(choices) % 2 == 0:
            sign = 1 if (i - 1) % 2 == 0 else -1
        else:
            sign = 1 if i % 2 == 0 else -1

        if not np.allclose(operator_expansions[0][choice], 0):
            val += sign * operator_expansions[0][choice] * coeff(operator_expansions[1:], other_choices)

    return val

def detcoeff(operator_expansions, choices):
    if len(operator_expansions) != len(choices):
        return 0
    if len(operator_expansions) == 0 or len(choices) == 0:
        return 0

    coeff_matrix = np.array(operator_expansions)
    submatrix = coeff_matrix[:, choices]

    return np.linalg.det(submatrix)


## TODO Add spin, check conversion of basis state object to choice



# What is the next step? First, we need to set up a reference state
# for the quantum state class. This will depend on the system (M, N).
# Then we need to convert an input BasisState to a choice that is
# valid for the above function, or rewrite it so it can take
# a basisstate.

# There is a coupling: the way we express the reference state
# and how the BasisState is converted to a choices. Does this
# mean that we need to write a class? How do we handle basis
# conversions in general? Should we write a class that contains
# all the possible bases and methods to convert between them
# rather than having separate Basis classes that then inevitably
# become tightly coupled?

# In this case, let us do something simple.


def positionstate_to_choices(state):
    # Use reshape
    choices = state.state.copy().reshape(-1)
    # Now find position indices that are nonzero
    indices = []
    for i in range(np.sum(choices)):
        place = np.argmax(choices)
        indices.append(place)
        choices[place] = 0

    return indices

# What are the allowed k-vectors?
# World is shape (M, N)
# kx = [0, ..., N - 1]
# ky = [0, ..., M - 1]

# How does k = (0, 0) expansion coeffs look?
# Normalization should not be important

def k_expansion(kx, ky, s, M, N, ns):
    xs = np.arange(N)
    ys = np.arange(M)
    X, Y = np.meshgrid(xs, ys)
    assert X.shape == (M, N)
    assert Y.shape == (M, N)
    expansion = np.exp(1j * kx * X + 1j * ky * Y)
    array = np.zeros((M, N, ns), dtype=np.complex128)
    array[:, :, s] = expansion
    return array.reshape(-1)


# Now we need to get a list of operator expansion
# for K particles in a (M, N) shaped world

def alternate(x):
    assert type(x) == int
    if x % 2 == 0:
        return -x // 2
    else:
        return (x + 1) // 2

def fermi_sea_expansions(K, M, N, ns):
    # Possible ks
    kxs = np.arange(-N//2, N//2)
    assert len(kxs) == N
    kys = np.arange(-M//2, M//2)
    assert len(kys) == M
    # Now we want to fill up the kvector spots
    # where we always take the lowest magnitude unfilled vectors
    KX, KY = np.meshgrid(kxs, kys)
    K_v = np.array([KX, KY])
    assert K_v.shape == (2, M, N), K_v.shape
    K2_xy = np.linalg.norm(K_v, axis=0)

    filled_indices = []
    if K > M * N:
        return filled_indices

    for n in range(K):
        indices = np.unravel_index(np.argmin(K2_xy), (M, N))
        spin = n % 2 if ns == 2 else 0
        index = (indices[0], indices[1], spin)

        filled_indices.append(index)
        if (n + 1) % 2 == 0 or ns == 1:
            K2_xy[indices] += np.max(K2_xy) * 100

    k_expansions = list(map(lambda ind: k_expansion(kxs[ind[1]], kys[ind[0]], ind[2], M, N, ns), filled_indices))

    return k_expansions


def tuplechoice_to_choice(tuple_choice, M, N, ns):
    # Turn a choice of coordinates (i, j)
    # into a choice valid for the recursive coeff function
    t = (tuple_choice[1], tuple_choice[0], tuple_choice[2])
    return np.ravel_multi_index(t, dims=(N, M, ns))


def build_FS_and_overlap(M, N, ns, K, choicetuples):
    exps = fermi_sea_expansions(K, M, N, ns)

    choices = list(map(lambda x: tuplechoice_to_choice(x, M, N, ns), choicetuples))
    
    return detcoeff(exps, choices)


def fullflow(M, N, ns, K, bstate):
    # This never changes, put it in QuantumState
    exps = fermi_sea_expansion(K, M, N, ns)

    choices = positionstate_to_choices(bstate)

    return detcoeff(exps, choices)

if __name__ == "__main__":
    # Test state conversion
    from basisstate import BasisState
    for nparts in range(0, 10):
        bstate = BasisState.random(5, 5, 2, nparts)
        choices = positionstate_to_choices(bstate)
        assert len(choices) == nparts
    exit()


    def gen_choicetuples(K, M, N, ns):
        choicetuples = []
        unique = True
        for _ in range(min(K, M * N * ns)):
            i = np.random.randint(0, M)
            j = np.random.randint(0, N)
            s = np.random.randint(0, ns)
            while (i, j, s) in choicetuples:
                i = np.random.randint(0, M)
                j = np.random.randint(0, N)
                s = np.random.randint(0, ns)
            if (i, j, ns) in choicetuples:
                unique = False
            choicetuples.append((i, j, s))

        return choicetuples, unique


    #print(fermi_sea_expansions(4, 2, 2))
    # exit()
    # print(list(map(lambda x: tuplechoice_to_choice(x, 5, 5, 2), gen_choicetuples(5, 5, 5, 2)[0])))
    # exit()

    nonzero = 0
    qwe = 0
    for M in range(15, 20):
        for N in range(15, 20):
            for K in range(0, 15):
                for z in range(10):
                    print("(M, N, K, z) = {}".format((M, N, K, z)), end="\r")
                    cts, unique = gen_choicetuples(K, M, N, 2)
                    overlap = build_FS_and_overlap(M, N, 2, K, cts)
                    if not unique:
                        assert np.allclose(overlap, 0)
                    else:
                        qwe += 1
                        if not np.allclose(overlap, 0):
                            nonzero += 1
                    
    print("NONZERO:", nonzero)
    print("QWE:", qwe)

    unique = False
    while not unique:
        c1, unique = gen_choicetuples(3, 10, 10, 2)

    ac1 = c1.copy()
    ac1[0] = c1[1]
    ac1[1] = c1[0]
    overlap0 = build_FS_and_overlap(10, 10, 3, 2, c1)
    overlap1 = build_FS_and_overlap(10, 10, 3, 2, ac1)
    assert np.allclose(overlap0, -overlap1), overlap1 / overlap0
                    
    exit()
                    
                


    for M in range(1, 20):
        for N in range(1, 20):
            for _ in range(100):
                rx = np.random.randint(M)
                ry = np.random.randint(N)
                index = tuplechoice_to_choice((rx, ry), M, N)
                expected = rx + ry * M
                assert index == expected
    exit()

    k_expansions = fermi_sea_expansions(2, 10, 10)
    for i, x in enumerate(k_expansions):
        ac1 = np.allclose(x[0, 0], x[0, 1])
        ac2 = np.allclose(x[0, 0], x[1, 0])
        if not ac1 and not ac2:
            assert i in [5, 6, 7, 8]
        else:
            assert i in [0, 1, 2, 3, 4, 9], i

    exit()

    # print(coeff([ck2, ck1, ck0], choices))
        
    s1 = [1, 0, 0]
    s2 = [1, 0, 0]

    for _ in range(10):
        random_choices = list(np.random.randint(3, size=2))
        assert np.allclose(coeff([s1, s2], random_choices), 0)

    choices = [0, 0]
    assert np.allclose(coeff([s1, s2], choices), 0)

    d1 = [1, 0, 0, 0]
    d2 = [0, 1, 0, 0]
    d3 = [0, 0, 1, 0]

    val = coeff([d3, d2, d1], [0, 1, 2])
    assert np.allclose(val, 1), val


    from basisstate import BasisState

    s = BasisState.random(10, 10, 1)

    print(positionstate_to_choices(s))
    print(s.state[:, :, 0].reshape(10, 10))
